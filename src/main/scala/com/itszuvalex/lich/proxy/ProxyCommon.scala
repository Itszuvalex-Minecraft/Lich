/*
 * ******************************************************************************
 *  * Copyright (C) 2013  Christopher Harris (Itszuvalex)
 *  * Itszuvalex@gmail.com
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *  *****************************************************************************
 */
package com.itszuvalex.lich.proxy

import net.minecraft.block.Block
import net.minecraft.item.Item
import net.minecraft.world.World

object ProxyCommon {
}

class ProxyCommon {

  def preInit(): Unit = {
  }

  def init(): Unit = {
  }

  def postInit(): Unit = {
    registerRendering()
    registerTileEntities()
    registerTickHandlers()
    registerEventHandlers()
  }

  def registerRendering(): Unit = {
  }

  def registerTileEntities(): Unit = {
  }

  def registerTickHandlers(): Unit = {
  }

  def registerEventHandlers(): Unit = {
  }

  def spawnParticle(world: World, name: String, x: Double, y: Double, z: Double, color: Int, velX: Double = 0d, velY: Double = 0d, velZ: Double = 0d): Object = {
    null
  }

  def onRegisterItem[T <: Item](item: T, name: String): Unit = {
  }

  def onRegisterBlock[T <: Block](block: T, name: String): Unit = {

  }
}
