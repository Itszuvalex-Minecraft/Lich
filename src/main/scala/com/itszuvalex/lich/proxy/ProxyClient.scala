/*
 * ******************************************************************************
 *  * Copyright (C) 2013  Christopher Harris (Itszuvalex)
 *  * Itszuvalex@gmail.com
 *  *
 *  * This program is free software; you can redistribute it and/or
 *  * modify it under the terms of the GNU General Public License
 *  * as published by the Free Software Foundation; either version 2
 *  * of the License, or (at your option) any later version.
 *  *
 *  * This program is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program; if not, write to the Free Software
 *  * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *  *****************************************************************************
 */
package com.itszuvalex.lich.proxy

import com.itszuvalex.lich.{Lich, LichItems}
import net.minecraft.block.Block
import net.minecraft.client.Minecraft
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.client.renderer.texture.TextureAtlasSprite
import net.minecraft.item.Item
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraftforge.client.event.TextureStitchEvent
import net.minecraftforge.client.model.obj.OBJLoader
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.relauncher.{Side, SideOnly}

@SideOnly(Side.CLIENT)
object ProxyClient extends ProxyCommon {
  var TEXTURE_RIFT_BILLBOARD: TextureAtlasSprite = _
}

@SideOnly(Side.CLIENT)
class ProxyClient extends ProxyCommon {
  override def spawnParticle(world: World, name: String, x: Double, y: Double, z: Double, color: Int, velX: Double, velY: Double, velZ: Double): Object = {
    null
  }

  override def preInit(): Unit = {
    super.preInit()
    LichItems.itemCallbacks += registerModels
  }

  def registerModels(): Unit = {
  }

  override def init(): Unit = {
    super.init()
    registerItemRendering()
  }

  def registerItemRendering(): Unit = {
  }

  override def registerRendering() {
    super.registerRendering()

    OBJLoader.INSTANCE.addDomain(Lich.ID.toLowerCase)

  }

  override def registerEventHandlers(): Unit = {
    super.registerEventHandlers()
  }

  override def onRegisterItem[T <: Item](item: T, name: String): Unit = {
    Minecraft.getMinecraft.getRenderItem.getItemModelMesher.register(item, 0, new ModelResourceLocation(new ResourceLocation(Lich.ID.toLowerCase(), name), "inventory"))
  }

  override def onRegisterBlock[T <: Block](block: T, name: String): Unit = {
    Minecraft.getMinecraft.getRenderItem.getItemModelMesher.register(Item.getItemFromBlock(block), 0, new ModelResourceLocation(Lich.ID.toLowerCase() + ":" + name, "inventory"))
  }

  @SubscribeEvent
  def handleTextureStitchPreEvent(event: TextureStitchEvent.Pre): Unit = {
  }
}