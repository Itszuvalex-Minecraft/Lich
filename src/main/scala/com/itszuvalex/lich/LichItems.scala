package com.itszuvalex.lich

import net.minecraft.item.Item
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.oredict.OreDictionary
import net.minecraftforge.registries.IForgeRegistry

import scala.collection.mutable.ArrayBuffer

object LichItems {

  val itemCallbacks = new ArrayBuffer[() => Unit]()

  @SubscribeEvent
  def registerItems(event: RegistryEvent.Register[Item]): Unit = {
    val registry = event.getRegistry

    LichBlocks.registerItemBlocks(registry)

    itemCallbacks.foreach(_ ())
    itemCallbacks.clear()
  }

  def registerItem[T <: Item](registry: IForgeRegistry[Item], item: T, name: String): T = {
    item.setCreativeTab(Lich.tab).setRegistryName(Lich.ID.toLowerCase(), name).setUnlocalizedName(name)
    registry.register(item)
    item
  }

  def init(): Unit = {
  }

  def postInit(): Unit = {

  }

  implicit class ItemHelpers[T <: Item](item: T) {
    def registerOre(name: String): T = {
      OreDictionary.registerOre(name, item)
      item
    }

    def registerModel(): Unit = {
      Lich.proxy.onRegisterItem(item, item.getUnlocalizedName.substring(5).toLowerCase())
    }
  }

}
