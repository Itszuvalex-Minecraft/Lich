package com.itszuvalex.lich

object LichRecipes {
  implicit def boxArray(array: Array[Any]): Array[Object] = array.map { case c: Char => c.asInstanceOf[Character]; case a: Object => a }

  def preInit(): Unit = {
  }

  def init(): Unit = {
    addSmeltingRecipes()
  }

  def addSmeltingRecipes(): Unit = {
  }

  def postInit(): Unit = {
  }
}
