package com.itszuvalex.lich

import com.itszuvalex.lich.api.ManagerLichCapabilities
import com.itszuvalex.lich.network.LichPacketHandler
import com.itszuvalex.lich.proxy.{ProxyCommon, ProxyGuiCommon}
import com.itszuvalex.lich.worldgen.LichOreGenerator
import net.minecraft.creativetab.CreativeTabs
import net.minecraft.init.Items
import net.minecraft.item.ItemStack
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.{FMLInitializationEvent, FMLPostInitializationEvent, FMLPreInitializationEvent}
import net.minecraftforge.fml.common.network.NetworkRegistry
import net.minecraftforge.fml.common.registry.GameRegistry
import net.minecraftforge.fml.common.{Mod, SidedProxy}
import org.apache.logging.log4j.LogManager

@Mod(modid = Lich.ID, name = "Lich", version = Lich.VERSION, modLanguage = "scala", dependencies = "required-after:itszulib")
object Lich {
  final val ID      = "lich"
  final val VERSION = Version.FULL_VERSION
  final val logger  = LogManager.getLogger(ID)

  val tab     : CreativeTabs   = new CreativeTabs(Lich.ID) {
    override def getTabIconItem: ItemStack = new ItemStack(Items.BONE)
  }
  @SidedProxy(clientSide = "com.itszuvalex.lich.proxy.ProxyClient",
    serverSide = "com.itszuvalex.lich.proxy.ProxyServer")
  var proxy   : ProxyCommon    = _
  @SidedProxy(clientSide = "com.itszuvalex.lich.proxy.ProxyGuiClient",
    serverSide = "com.itszuvalex.lich.proxy.ProxyGuiCommon")
  var guiProxy: ProxyGuiCommon = _

  @EventHandler def preInit(event: FMLPreInitializationEvent): Unit = {
    MinecraftForge.EVENT_BUS.register(LichSounds)
    MinecraftForge.EVENT_BUS.register(LichBlocks)
    MinecraftForge.EVENT_BUS.register(LichItems)
    MinecraftForge.EVENT_BUS.register(proxy)

    LichFluids.preInit()
    LichRecipes.preInit()

    LichPacketHandler.preInit()
    GameRegistry.registerWorldGenerator(new LichOreGenerator, LichOreGenerator.GENERATION_WEIGHT)
    NetworkRegistry.INSTANCE.registerGuiHandler(this, guiProxy)
    ManagerLichCapabilities.register()
    proxy.preInit()
  }

  @EventHandler def init(event: FMLInitializationEvent): Unit = {
    LichBlocks.init()
    LichItems.init()
    LichFluids.init()
    LichRecipes.init()
    proxy.init()
  }

  @EventHandler def postInit(event: FMLPostInitializationEvent): Unit = {
    LichBlocks.postInit()
    LichItems.postInit()
    LichFluids.postInit()
    LichRecipes.postInit()
    proxy.postInit()
  }
}
