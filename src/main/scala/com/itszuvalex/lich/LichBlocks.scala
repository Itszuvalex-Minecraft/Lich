package com.itszuvalex.lich

import net.minecraft.block.Block
import net.minecraft.item.{Item, ItemBlock}
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.oredict.OreDictionary
import net.minecraftforge.registries.IForgeRegistry

import scala.collection.mutable.ArrayBuffer

object LichBlocks {

  val blockCallbacks = new ArrayBuffer[() => Unit]()
  private val itemBlocksToRegister = new ArrayBuffer[(Block, String)]
  private val oresToRegister       = new ArrayBuffer[(Block, String)]

  @SubscribeEvent
  def registerBlocks(event: RegistryEvent.Register[Block]) {
    val registry = event.getRegistry

    blockCallbacks.foreach(_ ())
    blockCallbacks.clear()
  }

  def registerBlock[T <: Block](registry: IForgeRegistry[Block], block: T, name: String): T = {
    block.setCreativeTab(Lich.tab).setRegistryName(Lich.ID.toLowerCase, name).setUnlocalizedName(name)
    registry.register(block)
    itemBlocksToRegister += ((block, name))
    block
  }

  def init(): Unit = {
  }

  def postInit(): Unit = {

  }

  def registerItemBlocks(registry: IForgeRegistry[Item]): Unit = {
    itemBlocksToRegister.foreach { blockname =>
      registry.register(new ItemBlock(blockname._1).setRegistryName(blockname._1.getRegistryName).setUnlocalizedName(blockname._2))
    }
    itemBlocksToRegister.clear()

    oresToRegister.foreach { blockname =>
      OreDictionary.registerOre(blockname._2, blockname._1)
    }
    oresToRegister.clear()
  }

  implicit class BlockHelpers[T <: Block](block: T) {
    def registerOre(name: String): T = {
      oresToRegister += ((block, name))
      block
    }

    def registerModel(): Unit = {
      Lich.proxy.onRegisterBlock(block, block.getUnlocalizedName.substring(5))
    }
  }

}
