package com.itszuvalex.lich

import net.minecraft.util.ResourceLocation

/**
  * Created by Christopher Harris (Itszuvalex) on 9/17/15.
  */
object Resources {

  def Sound(loc: String): ResourceLocation = Lich(loc)

  def Lich(loc: String) = new ResourceLocation(com.itszuvalex.lich.Lich.ID.toLowerCase, loc)

  def TexBlock(name: String): ResourceLocation = Texture("blocks/" + name)

  def TexGui(name: String): ResourceLocation = Texture("guis/" + name)

  def TexItem(name: String): ResourceLocation = Texture("items/" + name)

  def Texture(name: String): ResourceLocation = Lich("textures/" + name)

  def Particle(name: String): ResourceLocation = Texture("particles/" + name)

  def CustomModelBlock(name: String): ResourceLocation = Lich("block/" + name)

  def CustomModelBlockTex(name: String): ResourceLocation = Lich("models/block/" + name)

  def ModelItem(name: String): ResourceLocation = Lich("item/" + name)
}
