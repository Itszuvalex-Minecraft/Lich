package com.itszuvalex.lich

import net.minecraft.util.SoundEvent
import net.minecraftforge.event.RegistryEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.registries.IForgeRegistry

import scala.collection.mutable.ArrayBuffer

object LichSounds {
  val soundCallbacks = new ArrayBuffer[() => Unit]()

  @SubscribeEvent def RegisterSound(event: RegistryEvent.Register[SoundEvent]): Unit = {
    val registry = event.getRegistry

    soundCallbacks.foreach(_ ())
    soundCallbacks.clear()
  }

  def registerSound(registry: IForgeRegistry[SoundEvent], name: String): SoundEvent = {
    val sound = new SoundEvent(Resources.Sound(name)).setRegistryName(name)
    registry.register(sound)
    SoundEvent.REGISTRY.getObject(Resources.Sound(name))
  }

  def addCallback(callback: () => Unit): Unit = soundCallbacks += callback
}
