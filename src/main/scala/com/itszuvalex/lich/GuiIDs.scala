package com.itszuvalex.lich

/**
  * Created by Christopher on 9/1/2015.
  */
object GuiIDs {

  private var n = 0

  private def nextID: Int = {
    n += 1
    n - 1
  }
}
